# Trees and Graphs: Breadth-First Search

<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

A graph is a **non-linear** data structure composed of two types of elements: **nodes** and their **connections** (relations). Each node represents an entity (a person, place, thing, category, or other piece of data), and each connection represents how two nodes are associated.

The biggest benefit of a graph structure is that it represents not only the nodes of the data but also their relationship to each other through the properties assigned to their connections. These relationships contain all the information necessary. Most importantly, you do not have to infer data connections from any external computation.

A classic example is Google Maps--it is just one big graph.

<img src="images/trees_and_graphs/breadth-first-search/bfs-1.png"  width="500">

It includes much more information than indicated in the illustration above: for example, the coordinates of a city, the direction of a street, the places located on a street, and so on.

---

<br>

There are two standard ways to represent a graph: as an adjacency list or as an adjacency matrix.

<img src="images/trees_and_graphs/breadth-first-search/bfs-2.png"  width="800">

We have an adjacency list given from the parameters.

---

In this challenge, we will implement an efficient graph traversal algorithm called as breadth-first search. A traversal is a systematic procedure for exploring each graph node by examining all of its adjacent nodes and connections.

BFS is so named because it expands first to the frontier nodes, then proceeds in rounds to group the nodes into levels. BFS starts at a given node (root) as **level 0**. In the first round, all nodes directly connected to the start node are placed into **level 1**. In the second round, it goes two steps away from the starting node, and new nodes are placed into **level 2**. This process continues until no new nodes are found to be placed in a level.

BFS is analogous to the ripple effect on water. The closest ring represents the frontier area to search. Then it goes to the second ring and then to the others:

<img src="images/trees_and_graphs/breadth-first-search/bfs-3.png"  width="800">

---

In basic BFS, we need two main variables:<br>
&emsp;\- A **queue** to perform the breadth-first order. **The first node discovered must be the first visited (first-in-first-out)**.<br>
&emsp;\- An array to mark whether or not each node has been discovered. In contrast to the tree data structure, graph nodes can be **cyclic**. We cannot allow any node to be visited over and over.<br>

In addition, we need to return an array to keep track of the path in which we proceed. So, we need another variable for the traversal path.

<b>`Hint:`&nbsp;Start from the node "0". Then, explore each node one by one in breadth-first order.<br>
<b>`Hint:`&nbsp;To achieve breadth-first order, dequeue (i.e. remove) the first element from the queue, then enqueue (i.e. add) each undiscovered node reachable from that node to the end of queue.<br>
<b>`Hint:`&nbsp;Loop the same process until there are no more undiscovered nodes.<br>

<b>`Note:`</b>&nbsp;You can use the built-in [**Array.shift**](https://apidock.com/ruby/Array/shift) function which removes the first element of an array to perform first-in-first-out.<br>

</details>
<!-- Hint 1 -->

---


<!-- Hint 2 -->
<details>
  <summary><b>Hint 2: [CODE]</b>&nbsp;A step-by-step explanation</summary><br>

The core structure of the algorithm includes the definition of the main variables and a loop to process each node seperately in breadth-first order.

```ruby
def bfs(graph)
  discovered = Array.new(graph.keys.length, false)
  queue = []
  path = []
  # Start with the node 0, push it to the end of the queue, and mark it discovered
  queue.push(0)
  discovered[0] = true
  
  until queue.empty?
    # Implement your solution here
  end
end
```

The algorithm flow of BFS is described in the illustration below. Inspect each step, then implement the required repetition.

<img src="images/trees_and_graphs/breadth-first-search/bfs-4.png"  width="800">
<img src="images/trees_and_graphs/breadth-first-search/bfs-5.png"  width="800">

<b>`Hint:`&nbsp;First, push the node "0" to the end of the queue.<br>
<b>`Hint:`&nbsp;At each loop step: <br>
&emsp;\- Dequeue (i.e. remove) the first element from the queue. Keep it; it is the current node.<br>
&emsp;\- Then, push the current node to the path array. We will return the path as the overall result.<br>
&emsp;\- Then, explore each adjacent node from the current node. Enqueue (i.e. add) each undiscovered adjacent node to the end of the queue and mark it discovered.<br>
<b>`Hint:`&nbsp;Continue through the loop until the queue is empty.<br>
</details>
<!-- Hint 2 -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution (Iterative)</summary><br>

```ruby
def bfs(graph)
  discovered = Array.new(graph.keys.length, false)
  queue = []
  path = []
  # Start with the node 0
  queue.push(0)
  # Mark the node 0 discovered
  discovered[0] = true
  until queue.empty?
    current = queue.shift
    # We're processing the current element at this point. So, push the current node to the result path 
    path.push(current)
    # Explore the adjacent nodes of the current node
    graph[current].each do |adjacent| 
      # If the adjacent node had not been discovered previously, push it to the end of the queue and mark it discoreved
      unless discovered[adjacent]
        queue.push(adjacent)
        discovered[adjacent] = true
      end
    end
  end
  # return the result path
  path
end
```

</details>
<!-- Solution -->


